﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public Text textTimer, textState,textCurrentPlayer,textLifePlayer;
    public Transform actionHUD, actionResult;
    public PlayerController[] players;
    public int currentPlayer = -1;
    int varLifePlayer,maVar=0;
    private PlayerIdentity playerIdentity;
    public GameObject p1, p2, p3, p4;
    string currentLifePlayer;

    public Transform nightHidePanel;

    internal void HideActionResult()
    {
        actionResult.gameObject.SetActive(false);
    }

    public string[] btnToActivate;
    public List<ActivateAction> btnActivated;

    private AudioSource day;
    private AudioSource dayStartSound;
    private AudioSource nightStartSound;
    private AudioSource night;


    public void Start()
    {
        textCurrentPlayer.text = "Current Player: 1";
        actionHUD.gameObject.SetActive(false);
        PlayerController.showHUDDelegate += ShowHUDDelegate;

        day = transform.Find("DaySound").GetComponent<AudioSource>();
        night = transform.Find("NightSound").GetComponent<AudioSource>();
        dayStartSound = transform.Find("DayStartSound").GetComponent<AudioSource>();
        nightStartSound = transform.Find("NightStartSound").GetComponent<AudioSource>();

        NightStateBehaviour.startNightDelegate += OnStartNight;
        NightStateBehaviour.endNightDelegate += OnEndNight;

        CountDownParty.gameOverDelegate += GameOver;
    }

    private void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }

    public void Update(){
        textCurrentPlayer.text = "Current Player: " + (currentPlayer+1);
        currentLifePlayer = GetCurrentPlayer().GetComponent<PlayerIdentity>().pointVie.ToString();
        textLifePlayer.text = "Player Life: "+currentLifePlayer;
    }

    private void OnStartNight()
    {
        Debug.Log("Nigth start");
        day.Pause();

        nightStartSound.Play();
        night.PlayDelayed(2f);
        nightHidePanel.gameObject.SetActive(true);
    }

    private void OnEndNight()
    {
        Debug.Log("Nigth end");
        night.Stop();

        dayStartSound.Play();
        StartCoroutine(UnPause(day, 2));
        nightHidePanel.gameObject.SetActive(false);
    }

    IEnumerator UnPause(AudioSource audio, float time)
    {
        yield return new WaitForSeconds(time);
        day.UnPause();
    }

    public void ShowHUDDelegate()
    {
        actionHUD.gameObject.SetActive(!actionHUD.gameObject.activeInHierarchy);
    }


    public void ShowActionResult(string text)
    {
        actionResult.gameObject.SetActive(true);
        actionResult.GetComponentInChildren<Text>().text = text;
    }

    internal void ActivateBtn(ActivateAction btn)
    {
        btnActivated.Add(btn);

        if (btnActivated.Count == btnToActivate.Length)
        {
            bool success = true;
            for (int i = 0; i < btnToActivate.Length; i++)
            {
                if (btnActivated[i].code != btnToActivate[i])
                {
                    success = false;
                }
            }
            if (!success)
            {
                foreach (ActivateAction action in btnActivated)
                {
                    action.Reset();
                }
                btnActivated.Clear();
                ShowActionResult("Tiens, les boutons viennent de se désactiver");
            }
            else
            {
                ShowActionResult("Oh, la porte est accessible");
                SceneManager.LoadScene("SceneEnd");
            }


        }
    }

    public void NextPlayerTurn()
    {
        if (currentPlayer != -1)
        {
            GetCurrentPlayer().SetActivePlayer(false);
            currentPlayer = (currentPlayer + 1) % players.Length;
        }
        else
        {
            InitPlayers();
        }
        GetCurrentPlayer().SetActivePlayer(true);
    }

    private void InitPlayers()
    {
        players = FindObjectsOfType<PlayerController>();
        currentPlayer = 0;
    }

    public PlayerController GetCurrentPlayer()
    {
        return players[currentPlayer];
    }


}
