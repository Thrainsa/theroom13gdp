﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{

    public Text text_NamePlayer1, text_NamePlayer2, text_NamePlayer3, text_NamePlayer4;
    public Text statusPlayer1, statusPlayer2, statusPlayer3, statusPlayer4;
    private int nbPlayer;
    private string namePlayer1, namePlayer2, namePlayer3, namePlayer4;
    public Image img1, img2, img3, img4;
    // Use this for initialization
    void Start()
    {

        nbPlayer = PlayerPrefs.GetInt("nbPlayer");
        //namePlayer1 = PlayerPrefs.GetString("namePlayer1");
        //namePlayer2 = PlayerPrefs.GetString("namePlayer2");
        //namePlayer3 = PlayerPrefs.GetString("namePlayer3");
        //namePlayer4 = PlayerPrefs.GetString("namePlayer4");

        text_NamePlayer1.text = "JASON";
        text_NamePlayer2.text = "Freddy";
        text_NamePlayer3.text = "Jigsaw";
        text_NamePlayer4.text = "Anabelle";




        Debug.Log("Nombre de player:" + nbPlayer);
    }

    // Update is called once per frame
    void Update()
    {
        //  affichage du nom des players
        text_NamePlayer1.text = namePlayer1;
        text_NamePlayer2.text = namePlayer2;
        text_NamePlayer3.text = namePlayer3;
        text_NamePlayer4.text = namePlayer4;

    }
}
