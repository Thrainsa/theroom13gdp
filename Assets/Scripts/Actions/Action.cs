﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Action : MonoBehaviour
{

    public string text;
    public ActionType type;

    protected MapController mapController;
    protected GameManager gameManager;

    public void Start()
    {
        this.mapController = FindObjectOfType<MapController>();
        this.gameManager = FindObjectOfType<GameManager>();
    }

    public virtual void Act(PlayerController player) { }

    public virtual void Reveal() { }
}
