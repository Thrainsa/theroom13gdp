﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateAction : Action {

    SpriteRenderer spriteRenderer;
    SpriteRenderer activateSpriteRenderer;
    AudioSource audio;

    public bool enableAtStart = false;
    public string alreadyActivated;

    public bool activated = false;
    public string code;

	// Use this for initialization
	public new void Start () {
        base.Start();

        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = enableAtStart;

        activateSpriteRenderer = transform.Find("activate").GetComponent<SpriteRenderer>();
        activateSpriteRenderer.enabled = false;

        audio = transform.Find("audio").GetComponent<AudioSource>();
    }

    public override void Act(PlayerController player) {
        if (spriteRenderer.enabled)
        {
            if (activated)
            {
                gameManager.ShowActionResult(alreadyActivated);
            }
            else
            {
                audio.Play();
                activated = true;
                gameManager.ShowActionResult(text);
                spriteRenderer.enabled = false;
                activateSpriteRenderer.enabled = true;
                gameManager.ActivateBtn(this);
            }
        }
    }

    public override void Reveal() {
        spriteRenderer.enabled = true;
    }

    internal void Reset()
    {
        activated = false;
        spriteRenderer.enabled = true;
        activateSpriteRenderer.enabled = false;
    }
}
