﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FouillerAction : Action
{
    public Action[] actions;

    public new void Start()
    {
        base.Start();
        actions = GetComponents<Action>();
    }

    public override void Act(PlayerController player)
    {
        gameManager.ShowActionResult(text);
        foreach(Action action in actions){
            action.Reveal();
        }
    }

}
