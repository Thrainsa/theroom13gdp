﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDownParty : MonoBehaviour
{

    public delegate void OnGameOverDelegate();
    public static event OnGameOverDelegate gameOverDelegate;

    GameManager gameManager;
    Animator animator;

    bool init = false;

    public float maxTime;
    float timeLeft;

    public int minDayTime;
    public int maxDayTime;

    public int minNightTime;
    public int maxNightTime;

    float nextState = 20f;
    public bool day = true;

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    public void FixedUpdate()
    {
        if (init)
        {
            OnStateUpdate();
        }
    }


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public void StartCountDown(Animator animator)
    {
        this.animator = animator;
        timeLeft = maxTime;
        gameManager.textState.text = "Jour";
        init = true;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    public void OnStateUpdate()
    {
        if (timeLeft > 0)
        {
            timeLeft -= Time.fixedDeltaTime;
            string minutes = ((int)timeLeft / 60).ToString();
            string secondes = (timeLeft % 60).ToString("f2");
            gameManager.textTimer.text = minutes + ":" + secondes;

            //pour le changement d'état (jour/nuit)
            if (Time.time > nextState)
            {
                changeState();
            }

        }

    }

    public void changeState()
    {
        day = !day;
        if (day)
        {
            gameManager.textState.text = "Jour";
            nextState = nextState + Random.Range(minDayTime, maxDayTime);
        }
        else
        {
            gameManager.textState.text = "Nuit";
            nextState = nextState + Random.Range(minNightTime, maxNightTime);
        }
        animator.SetTrigger("changeState");
        Debug.Log(gameManager.textState.text);
    }
}
