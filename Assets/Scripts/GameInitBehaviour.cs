﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInitBehaviour : StateMachineBehaviour {

    CountDownParty countDownParty;

    private void Start()
    {
    }

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        // TODO init
        countDownParty = FindObjectOfType<CountDownParty>();
        countDownParty.StartCountDown(animator);
        animator.SetTrigger("next");
    }



}
