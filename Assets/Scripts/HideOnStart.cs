﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class HideOnStart : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<TilemapRenderer>().enabled = false;
    }
	
}
