﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractableTile : MonoBehaviour
{
    public bool isBlocking = false;
    public int layoutOrder = 3;

    private MapController mapController;
    public List<Action> actions = new List<Action>();

    void Start()
    {
        mapController = FindObjectOfType<MapController>();
        mapController.AddInteractableAt(this, transform.position);
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        if (spriteRenderer != null)
        {
            spriteRenderer.sortingOrder = layoutOrder;
        }

        foreach(Action action in GetComponentsInChildren<Action>())
        {
            actions.Add(action);
        }

    }

    public bool moveTo(Vector3 destinationPosition,PlayerController playerController)
    {

        return mapController.moveTo(this, transform.position, destinationPosition);
    }

}
