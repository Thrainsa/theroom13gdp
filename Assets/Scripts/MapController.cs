﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    public GameManager gameManager;

    private Dictionary<Vector3, List<InteractableTile>> interactables = new Dictionary<Vector3, List<InteractableTile>>();

    public List<InteractableTile> GetInteractablesAt(Vector3 position)
    {
        List<InteractableTile> res;
        interactables.TryGetValue(position, out res);
        return res;
    }

    public void AddInteractableAt(InteractableTile interactableTile, Vector3 position)
    {
        if (!interactables.ContainsKey(position))
        {
            interactables.Add(position, new List<InteractableTile>());
        }
        interactables[position].Add(interactableTile);
    }

    public void Act(PlayerController player, Vector3 destination, ActionType actionType)
    {
        List<InteractableTile> interactables = GetInteractablesAt(destination);
        bool act = false;
        if (interactables != null)
        {
            foreach (InteractableTile interactable in interactables)
            {
                foreach (Action action in interactable.actions)
                {
                    if(action.type == actionType)
                    {
                        action.Act(player);
                        act = true;
                        break;
                    }
                }
                if (act)
                {
                    break;
                }
            }
        }
        if(ActionType.A == actionType && !act)
        {
            gameManager.ShowActionResult("Il n'y a rien d'intéressant ici");
        }
    }

    public bool CanMoveTo(Vector3 position)
    {
        List<InteractableTile> interactables = GetInteractablesAt(position);
        if (interactables != null)
        {
            foreach (InteractableTile interactable in interactables)
            {
                if (interactable.isBlocking)
                {
                    return false;
                }
            }
        }
        return true;
    }

    public bool moveTo(InteractableTile interactable, Vector3 currentPosition, Vector3 destinationPosition)
    {
        if(CanMoveTo(destinationPosition))
        {
            interactables[currentPosition].Remove(interactable);
            AddInteractableAt(interactable, destinationPosition);
            return true;
        }
        else
        {
            return false;
        }
    }
}
