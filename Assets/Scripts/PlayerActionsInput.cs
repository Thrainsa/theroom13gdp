﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionsInput : PlayerActionSet
{

    //Drirectional Actions; 
    public PlayerAction left;
    public PlayerAction right;
    public PlayerAction up;
    public PlayerAction down;
    public PlayerTwoAxisAction move;
    
    public PlayerAction interract;
    public PlayerAction push;
    public PlayerAction search;
    public PlayerAction next;

    public PlayerAction showHUD;



    /// <summary>
    /// initialise toute les methodes d'action au niveau input.
    /// </summary>
    public PlayerActionsInput()
    {
        left = CreatePlayerAction("Move left");
        right = CreatePlayerAction("Move right");
        up = CreatePlayerAction("Move up");
        down = CreatePlayerAction("Move down");
        move = CreateTwoAxisPlayerAction(left, right, down, up);

        interract = CreatePlayerAction("Interact_action");
        push = CreatePlayerAction("Push_action");
        search = CreatePlayerAction("Search_action");
        next = CreatePlayerAction("Next_action");

        showHUD = CreatePlayerAction("Show_HUD");
    }
}