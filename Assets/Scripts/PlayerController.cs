﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public int maxMouvement = 3;
    public bool activePlayer = false;

    public delegate void OnEndTurnDelegate();
    public static event OnEndTurnDelegate endTurnDelegate;

    public delegate void OnShowHUDDelegate();
    public static event OnShowHUDDelegate showHUDDelegate;

    Animator animator;
    InteractableTile interactable;
    MapController mapController;
    PlayerControllerInput inputController;
    CountDownParty countDownParty;

    List<Vector3> destinationPoints = new List<Vector3>();
    Vector3 target;
    bool resetNextMovement = true;
    private int turnMovement = 0;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        interactable = GetComponent<InteractableTile>();
        mapController = FindObjectOfType<MapController>();
        countDownParty = FindObjectOfType<CountDownParty>();
        inputController = GetComponent<PlayerControllerInput>();
        target = transform.position;
        destinationPoints.Add(new Vector3(target.x, target.y, 0));
        // Debug.Log(inputController.myActions.Device.Meta);
    }

    // Update is called once per frame
    void Update()
    {
        if (activePlayer || !countDownParty.day)
        {
            if (!animator.GetBool("isWalking") && countDownParty.day)
            {
                HandleChooseActions();
            }
            HandleChooseMovement();
        }
        HandleMoveToTarget();
    }

    void HandleChooseActions()
    {
        bool action = inputController.myActions.search.WasPressed;
        if (action)
        {
            // TODO action type
            Debug.Log("search WasPressed");
            mapController.Act(this, transform.position, ActionType.A);
            mapController.Act(this, GetDestination(animator.GetFloat("input_x"), animator.GetFloat("input_y"), transform.position), ActionType.A);
        }

        action = inputController.myActions.interract.WasPressed;
        if (action)
        {
            // TODO action type
            Debug.Log("interract WasPressed");
            mapController.Act(this, transform.position, ActionType.X);
            mapController.Act(this, GetDestination(animator.GetFloat("input_x"), animator.GetFloat("input_y"), transform.position), ActionType.X);
        }

        action = inputController.myActions.next.WasPressed;
        if (action)
        {
            endTurnDelegate();
        }

        action = inputController.myActions.showHUD.WasPressed;
        if (action)
        {
            showHUDDelegate();
        }
    }

    void HandleChooseMovement() {
        float h = inputController.myActions.move.X;
        float v = inputController.myActions.move.Y;

        if (resetNextMovement)
        {
            if (Mathf.Abs(h) > 0.1 || Mathf.Abs(v) > 0.1)
            {
                resetNextMovement = false;
                Vector3 lastDest = destinationPoints[destinationPoints.Count - 1];
                Vector3 newDest = GetDestination(h, v, lastDest);
                if (destinationPoints.Count > 1 && destinationPoints[destinationPoints.Count - 2] == newDest)
                {
                    destinationPoints.RemoveAt(destinationPoints.Count - 1);
                    turnMovement--;
                }
                else if(turnMovement < maxMouvement || !countDownParty.day)
                {
                    turnMovement++;
                    destinationPoints.Add(newDest);
                } else if(turnMovement >= maxMouvement && !animator.GetBool("isWalking"))
                {
                    mapController.gameManager.ShowActionResult("Tiens, je ne peux plus bouger :/ (Press Y) ");
                }
            }
        }
        else if (Mathf.Abs(h) < 0.1 && Mathf.Abs(v) < 0.1)
        {
            resetNextMovement = true;
        }

    }

    void HandleMoveToTarget() {
        if (target == transform.position && destinationPoints.Count > 1)
        {
            if (!enabled)
            {
                destinationPoints.Clear();
                destinationPoints.Add(target);
            }
            else
            {
                destinationPoints.RemoveAt(0);
                target = destinationPoints[0];
                if (target != null)
                {
                    bool canMove = interactable.moveTo(target, this);
                    if (!canMove)
                    {
                        animator.SetFloat("input_x", target.x - transform.position.x);
                        animator.SetFloat("input_y", target.y - transform.position.y);
                        destinationPoints.Clear();
                        target = transform.position;
                        destinationPoints.Add(target);
                    }
                }
            }
        }

        if (target != null && target != transform.position)
        {
            animator.SetBool("isWalking", true);
            animator.SetFloat("input_x", target.x - transform.position.x);
            animator.SetFloat("input_y", target.y - transform.position.y);
            transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * 2);
        }
        else
        {
            animator.SetBool("isWalking", false);
        }
        // Debug.Log("Anim -> input_x: " + animator.GetFloat("input_x") + "input_y: " + animator.GetFloat("input_y") + "isWalking: " + animator.GetBool("isWalking"));
    }

    private static Vector3 GetDestination(float h, float v, Vector3 lastDest)
    {
        Vector3 newDest;
        if (Mathf.Abs(h) > Mathf.Abs(v))
        {
            // Move horizontally
            newDest = new Vector3(lastDest.x + (h < 0 ? -1 : 1), lastDest.y);
        }
        else
        {
            // Move vertically
            newDest = new Vector3(lastDest.x, lastDest.y + (v < 0 ? -1 : 1));
        }

        return newDest;
    }

    public void SetActivePlayer(bool active)
    {
        this.activePlayer = active;
        turnMovement = 0;
    }
}
