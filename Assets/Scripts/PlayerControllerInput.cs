﻿using InControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerInput : MonoBehaviour
{

    public PlayerActionsInput myActions;
    int playerInputNumber;
    int nbPlayer;// nombre de joueur

    // Use this for initialization
    void Start()
    {
        playerInputNumber = gameObject.GetComponent<PlayerIdentity>().playerIndex;
        myActions = new PlayerActionsInput();
       // Debug.Log("Nombre de player:"+InputManager.Devices.Count);
        nbPlayer = InputManager.Devices.Count;
        PlayerPrefs.SetInt("nbPlayer", nbPlayer);
        if (InputManager.Devices.Count != 0)
        {

            myActions.Device = InputManager.Devices[playerInputNumber];
            Debug.Log(playerInputNumber + "  :   "+ myActions.Device.Name);
           
        }


        #region 
        myActions.down.AddDefaultBinding(InputControlType.LeftStickDown);
        
        myActions.up.AddDefaultBinding(InputControlType.LeftStickUp);

        myActions.right.AddDefaultBinding(InputControlType.LeftStickRight);

        myActions.left.AddDefaultBinding(InputControlType.LeftStickLeft);
        #endregion

        myActions.search.AddDefaultBinding(InputControlType.Action1);
        myActions.push.AddDefaultBinding(InputControlType.Action2);
        myActions.interract.AddDefaultBinding(InputControlType.Action3);
        myActions.next.AddDefaultBinding(InputControlType.Action4);

        myActions.showHUD.AddDefaultBinding(InputControlType.Command);
    }
}
