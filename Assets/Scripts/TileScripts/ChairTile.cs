﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;

#if UNITY_EDITOR
using UnityEditor;
#endif


public class ChairTile : SpriteRendererTile
{

#if UNITY_EDITOR
    [MenuItem("Assets/Create/Tiles/ChairTile")]
    public static void CreateChairTile()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Chair Tile", "New Chair Tile", "asset", "Save Chair Tile", "Assets");
        if (path == "")
            return;

        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<ChairTile>(), path);
    }
#endif

}
