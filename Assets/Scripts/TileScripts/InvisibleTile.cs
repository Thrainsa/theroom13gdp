﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;

#if UNITY_EDITOR
using UnityEditor;
#endif


public class InvisibleTile : Tile
{

#if UNITY_EDITOR
    [MenuItem("Assets/Create/Tiles/InvisibleTile")]
    public static void CreateInvisibleTile()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Invisible Tile", "New Invisible Tile", "asset", "Save Invisible Tile", "Assets");
        if (path == "")
            return;

        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<InvisibleTile>(), path);
    }
#endif

}
