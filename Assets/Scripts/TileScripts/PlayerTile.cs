﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;

#if UNITY_EDITOR
using UnityEditor;
#endif


public class PlayerTile : Tile {

#if UNITY_EDITOR
	[MenuItem("Assets/Create/Tiles/PlayerTile")]
    public static void CreatePlayerTile() 
	{
		string path = EditorUtility.SaveFilePanelInProject("Save Player Tile", "New Player Tile", "asset", "Save Player Tile", "Assets");
		if (path == "")
			return;

		AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<PlayerTile>(), path);
	}
#endif

}
