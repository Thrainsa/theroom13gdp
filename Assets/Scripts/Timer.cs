﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public Text timerText;
    public float startTimer;

	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        float t = startTimer - Time.time;
        
        string minutes = ((int)t / 60).ToString();
        string secondes = (t % 60).ToString("f2");
        timerText.text = minutes + ":" + secondes;
        finishTimer();
	}

    public void finishTimer()
    {
       
        Debug.Log("Time's Up");
    }
}
