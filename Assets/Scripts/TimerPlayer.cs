﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerPlayer : MonoBehaviour
{

    public Text timerText;
    public float startTimer,maxTime;

    void Start()
    {
        startTimer = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        
        float t = Time.time - startTimer;

        string minutes = ((int)t / 60).ToString();
        string secondes = (t % 60).ToString("f2");
        timerText.text = minutes + ":" + secondes;
        finishTimer();
    }

    public void finishTimer()
    {

        Debug.Log("Time's Up");
    }
}
